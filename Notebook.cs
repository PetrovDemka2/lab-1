﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1.Petrov_Demid
{
    class Notebook
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public long PhoneNumber { get; set; }
        public string Country { get; set; }
        public string Birthday { get; set; }
        public string Organiztion { get; set; }
        public string Post { get; set; }
        public string ExtraInfo { get; set; }


        public Notebook()
        {
            Console.Write("Введите фамилию: ");
            Surname = Console.ReadLine().Trim();
            while (string.IsNullOrEmpty(Surname))
            {
                Console.Write("Это поле обязательно к заполнению! Введите фамилию: ");
                Surname = Console.ReadLine().Trim();
            }

            Console.Write("Введите имя: ");
            Name = Console.ReadLine().Trim();
            while (string.IsNullOrEmpty(Name))
            {
                Console.Write("Это поле обязательно к заполнению! Введите имя: ");
                Name = Console.ReadLine().Trim();

            }

            Console.Write("*Введите отчество: ");
            Patronymic = Console.ReadLine().Trim();

            long phoneNumber;
            Console.Write("Введите номер телефона (без +): ");
            bool isNumberCorrect = long.TryParse(Console.ReadLine().Trim(), out phoneNumber);
            PhoneNumber = phoneNumber;
            while (!isNumberCorrect)
            {
                Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                Console.Write("Введите номер снова: ");
                isNumberCorrect = long.TryParse(Console.ReadLine().Trim(), out phoneNumber);
                PhoneNumber = phoneNumber;
            }

            Console.Write("Введите страну: ");
            Country = Console.ReadLine().Trim();
            while (string.IsNullOrEmpty(Country))
            {
                Console.Write("Это поле обязательно к заполнению! Введите страну: ");
                Country = Console.ReadLine().Trim();
            }

            Console.Write("*Введите дату рождения в формате: ");
            Birthday = Console.ReadLine().Trim();


            Console.Write("*Введите организацию: ");
            Organiztion = Console.ReadLine().Trim();

            Console.Write("*Введите должность: ");
            Post = Console.ReadLine().Trim();

            Console.Write("*Введите заметки: ");
            ExtraInfo = Console.ReadLine().Trim();
        }
    }
}
