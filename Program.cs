﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Lab_1.Petrov_Demid
{
    class Program
    {
        public static Dictionary<int, Notebook> phoneNumberBook = new Dictionary<int, Notebook>();
        private static int idNote = 1;
        private static int count = 0;
        static void Main(string[] args)
        {

            Console.Title = "Записная книжка";
            Console.WriteLine("Добро пожаловать в записную книжку!");
            bool isActionCorrect = false;
            while (!isActionCorrect)
            {
                Console.WriteLine("Выбирете действие с записями: (Создать/Редактировать/Удалить/Просмотреть/Просмотреть все/Выход)");
                string action = Console.ReadLine().ToLower().Trim();
                switch (action)
                {
                    case "создать": Create(); break;
                    case "редактировать": Edit(); break;
                    case "удалить": Delete(); break;
                    case "просмотреть": Show(); break;
                    case "просмотреть все": ShowAll(); break;
                    case "выход": isActionCorrect = true; Console.WriteLine("До свидания!"); break;
                    default: isActionCorrect = false; Console.WriteLine("Вы ввели что-то не то! Попробуйте еще раз:"); break;
                }
            }


        }

        public static void Create()
        {
            Console.Clear();
            Console.WriteLine("Создание новой записи (Необязательные поля помечены *)");
            Console.WriteLine();
            phoneNumberBook.Add(idNote, new Notebook());
            Console.WriteLine($"Запись успешно создана! Id записи - {idNote}.");
            Console.WriteLine();
            Console.WriteLine("Нажмите любую клавишу для продолжения.");
            Console.ReadLine();
            Console.Clear();

            idNote++;
            count++;
        }
        public static void Edit()
        {
            Console.Clear();
            if (count == 0)
            {
                Console.WriteLine("Нет созданных записей!");
                Console.WriteLine();
                Console.WriteLine("Нажмите любую клавишу для продолжения.");
                Console.ReadLine();
                Console.Clear();
                return;
            }

            Console.WriteLine("Редактирование записи");
            Console.WriteLine();

            Console.Write("Введите id записи, которую хотите изменить: ");
            int id;
            bool isIdCorrect = int.TryParse(Console.ReadLine(), out id);
            while (!isIdCorrect)
            {
                Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                Console.Write("Введите id снова: ");
                isIdCorrect = int.TryParse(Console.ReadLine(), out id);
                Console.WriteLine();
            }



            bool isIdValid = phoneNumberBook.ContainsKey(id);
            while (!isIdValid)
            {
                Console.WriteLine("Записи с таким id не существует!");
                Console.Write("Введите id снова: ");
                bool isIdCorrect1 = int.TryParse(Console.ReadLine(), out id);
                while (!isIdCorrect1)
                {
                    Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                    Console.Write("Введите id снова: ");
                    isIdCorrect1 = int.TryParse(Console.ReadLine(), out id);
                    Console.WriteLine();
                }
                isIdValid = phoneNumberBook.ContainsKey(id);
            }
            Console.WriteLine();


            bool isDataCorrect = false;
            while (!isDataCorrect)
            {
                Console.WriteLine("Что хотите отредактировать? (Фамилия/Имя/Отчество/Номер телефона/Страна/Дата рождения/Организация/Должность/Заметки/Ничего)");
                string data = Console.ReadLine().ToLower().Trim();

                switch (data)
                {
                    case "фамилия":
                        Console.Write("Введите новую фамилию: ");
                        string surname = Console.ReadLine().Trim();
                        while (string.IsNullOrEmpty(surname))
                        {
                            Console.WriteLine("Вы ничего не ввели! Введите фамилию: ");
                            surname = Console.ReadLine().Trim();
                        }
                        phoneNumberBook[id].Surname = surname;
                        Console.WriteLine("Фамилия успешно изменена!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "имя":
                        Console.Write("Введите новое имя: ");
                        string name = Console.ReadLine().Trim();
                        while (string.IsNullOrEmpty(name))
                        {
                            Console.WriteLine("Вы ничего не ввели! Введите имя: ");
                            name = Console.ReadLine().Trim();
                        }
                        phoneNumberBook[id].Name = name;
                        Console.WriteLine("Имя успешно изменено!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "отчество":
                        Console.Write("Введите новое отчество: ");
                        phoneNumberBook[id].Patronymic = Console.ReadLine().Trim();
                        Console.WriteLine("Отчество успешно изменено!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "номер телефона":
                        Console.WriteLine("Введите новый номер (без +): ");
                        long phoneNumber;
                        bool isNumberCorrect = long.TryParse(Console.ReadLine().Trim(), out phoneNumber);
                        phoneNumberBook[id].PhoneNumber = phoneNumber;
                        while (!isNumberCorrect)
                        {
                            Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                            Console.Write("Введите новый номер снова: ");
                            isNumberCorrect = long.TryParse(Console.ReadLine().Trim(), out phoneNumber);
                            phoneNumberBook[id].PhoneNumber = phoneNumber;
                        }
                        Console.WriteLine("Номер телефона успешно изменен!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "страна":
                        Console.Write("Введите новую страну: ");
                        string country = Console.ReadLine().Trim();
                        while (string.IsNullOrEmpty(country))
                        {
                            Console.WriteLine("Вы ничего не ввели! Введите страну: ");
                            country = Console.ReadLine().Trim();
                        }
                        phoneNumberBook[id].Country = country;
                        Console.WriteLine("Страна успешно изменена!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "дата рождения":
                        Console.Write("Введите новую дату рождения: ");
                        phoneNumberBook[id].Birthday = Console.ReadLine().Trim();
                        Console.WriteLine("Дата рождения успешно изменена!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "организация":
                        Console.Write("Введите новую организацию: ");
                        phoneNumberBook[id].Organiztion = Console.ReadLine().Trim();
                        Console.WriteLine("Организация успешно изменена!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "должность":
                        Console.Write("Введите новую должность: ");

                        phoneNumberBook[id].Post = Console.ReadLine().Trim();
                        Console.WriteLine("Должность успешно изменена!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "заметки":
                        Console.Write("Введите новые заметки: ");

                        phoneNumberBook[id].ExtraInfo = Console.ReadLine().Trim();
                        Console.WriteLine("Заметки успешно изменены!");
                        Console.WriteLine();
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "ничего":
                        isDataCorrect = true;
                        Console.WriteLine("Нажмите любую клавишу для продолжения.");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    default: isDataCorrect = false; Console.WriteLine("Вы ввели что-то не то! Попробуйте еще раз:"); break;
                }
            }

        }
        public static void Delete()
        {
            Console.Clear();
            if (count == 0)
            {
                Console.WriteLine("Нет созданных записей!");
                Console.WriteLine();
                Console.WriteLine("Нажмите любую клавишу для продолжения.");
                Console.ReadLine();
                Console.Clear();
                return;
            }
            Console.WriteLine("Удаление записи");
            Console.WriteLine();

            Console.Write("Введите id записи, которую хотите удалить: ");
            int id;
            bool isIdCorrect = int.TryParse(Console.ReadLine(), out id);
            while (!isIdCorrect)
            {
                Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                Console.Write("Введите id снова: ");
                isIdCorrect = int.TryParse(Console.ReadLine(), out id);
                Console.WriteLine();
            }

            bool isIdValid = phoneNumberBook.ContainsKey(id);
            while (!isIdValid)
            {
                Console.WriteLine("Записи с таким id не существует!");
                Console.Write("Введите id снова: ");
                bool isIdCorrect1 = int.TryParse(Console.ReadLine(), out id);
                while (!isIdCorrect1)
                {
                    Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                    Console.Write("Введите id снова: ");
                    isIdCorrect1 = int.TryParse(Console.ReadLine(), out id);
                    Console.WriteLine();
                }
                isIdValid = phoneNumberBook.ContainsKey(id);
            }
            Console.WriteLine();

            phoneNumberBook.Remove(id);
            Console.WriteLine("Запись успешно удалена!");
            Console.WriteLine();
            Console.WriteLine("Нажмите любую клавишу для продолжения.");
            Console.ReadLine();
            Console.Clear();
            count--;
        }
        public static void Show()
        {
            Console.Clear();
            if (count == 0)
            {
                Console.WriteLine("Нет созданных записей!");
                Console.WriteLine();
                Console.WriteLine("Нажмите любую клавишу для продолжения.");
                Console.ReadLine();
                Console.Clear();
                return;
            }

            Console.WriteLine("Просмотр записи");
            Console.WriteLine();

            Console.Write("Введите id записи, которую хотите просмотерть: ");
            int id;
            bool isIdCorrect = int.TryParse(Console.ReadLine(), out id);
            while (!isIdCorrect)
            {
                Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                Console.Write("Введите id снова: ");
                isIdCorrect = int.TryParse(Console.ReadLine(), out id);
                Console.WriteLine();
            }

            bool isIdValid = phoneNumberBook.ContainsKey(id);
            while (!isIdValid)
            {
                Console.WriteLine("Записи с таким id не существует!");
                Console.Write("Введите id снова: ");
                bool isIdCorrect1 = int.TryParse(Console.ReadLine(), out id);
                while (!isIdCorrect1)
                {
                    Console.WriteLine("Вы ничего не ввели, либо ввели не цифры!");
                    Console.Write("Введите id снова: ");
                    isIdCorrect1 = int.TryParse(Console.ReadLine(), out id);
                    Console.WriteLine();
                }
                isIdValid = phoneNumberBook.ContainsKey(id);
            }
            Console.WriteLine();
            Console.WriteLine($"Запись {id}");
            Console.WriteLine($"Фамилия: {phoneNumberBook[id].Surname}");
            Console.WriteLine($"Имя: {phoneNumberBook[id].Name}");
            Console.WriteLine($"Отчество: {phoneNumberBook[id].Patronymic}");
            Console.WriteLine($"Номер телефона: {phoneNumberBook[id].PhoneNumber}");
            Console.WriteLine($"Страна: {phoneNumberBook[id].Country}");
            Console.WriteLine($"Дата рождения: {phoneNumberBook[id].Birthday}");
            Console.WriteLine($"Организация: {phoneNumberBook[id].Organiztion}");
            Console.WriteLine($"Должность: {phoneNumberBook[id].Post}");
            Console.WriteLine($"Заметки: {phoneNumberBook[id].ExtraInfo}");

            Console.WriteLine();
            Console.WriteLine("Нажмите любую клавишу для продолжения.");
            Console.ReadLine();
            Console.Clear();
        }
        public static void ShowAll()
        {
            Console.Clear();
            if (count == 0)
            {
                Console.WriteLine("Нет созданных записей!");
                Console.WriteLine();
                Console.WriteLine("Нажмите любую клавишу для продолжения.");
                Console.ReadLine();
                Console.Clear();
                return;
            }
            Console.WriteLine("Все записи");
            Console.WriteLine();
            foreach (var item in phoneNumberBook)
            {
                Console.WriteLine($"Запись с id {item.Key}");
                Console.WriteLine($"Фамилия: {item.Value.Surname}");
                Console.WriteLine($"Имя: {item.Value.Name}");
                Console.WriteLine($"Номер телефона: {item.Value.PhoneNumber}");
                Console.WriteLine();
            }
            Console.WriteLine("Нажмите любую клавишу для продолжения.");
            Console.ReadLine();
            Console.Clear();
        }
    }
}
